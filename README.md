# How to use

## Basic Usage

After downloading, simply edit the HTML and CSS files included with dist directory. These are the only files you need to worry about, you can ignore everything else! To preview the changes you make to the code, you can open the index.html file in your web browser.

## Advanced Usage

### Prerequisites

> You must have ```npm``` installed in order to use this build environment.

Clone the source files of the theme and navigate into the theme's root directory.  
```Run npm install``` and then run ```npm start``` which will open up a preview of the template in your default browser, watch for changes to core template files, and live reload the browser when changes are saved.  
You can view the package.json file to see which scripts are included.

## Thanks

[startbootstrap-freelancer](https://github.com/BlackrockDigital/startbootstrap-freelancer) for theme

[Gitlab](https://gitlab.com) for hosting!
